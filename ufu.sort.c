
#include "ufu.h"
#include "ufu.p"

void ufu_sort(struct s_env *env,int panel) {

  char sortc,sorto;
  struct s_entry *tos;

  if(strlen(env->panel[panel]->dirname)>0) {

    tos=env->panel[panel]->tos;

    switch(env->panel[panel]->sortc) {
      case UFU_SORT_FNAME:
        sortc='f';
        break;
      case UFU_SORT_SIZE:
        sortc='s';
        break;
      case UFU_SORT_LA:
        sortc='a';
        break;
      case UFU_SORT_LM:
        sortc='m';
        break;
      case UFU_SORT_LC:
        sortc='c';
        break;
    }

    switch(env->panel[panel]->sorto) {
      case UFU_SORT_ASC:
        sorto='a';
        break;
      case UFU_SORT_DSC:
        sorto='d';
        break;
    }

    sprintf(env->msg," Sorting panel #%d (%s,%c%c).",panel,env->panel[panel]->dirname,sortc,sorto);
    ufu_log(env);

    ufu_listsort(env,panel);

    if(env->dirfirst) {
      ufu_shuffle(env,panel);
    }

    env->panel[panel]->cos=tos;
    env->panel[panel]->tos=tos;

    sprintf(env->msg," Finished sorting panel #%d.",panel);
    ufu_log(env);

  }

}

void ufu_sort_swap(struct s_env *env,struct s_entry *x,struct s_entry *y) {

  struct s_entry *t;

  t=ufu_alloc_entry(env);

  ufu_free_string(env,t->fname);
  ufu_free_string(env,t->fperm);
  ufu_free_string(env,t->lname);
  ufu_free_string(env,t->lperm);

  ufu_free_string(env,t->fusrname);
  ufu_free_string(env,t->fgrpname);
  ufu_free_string(env,t->lusrname);
  ufu_free_string(env,t->lgrpname);

  //t->seqno=x->seqno;
  t->fstat=x->fstat;
  t->lstat=x->lstat;
  t->fname=x->fname;
  t->fperm=x->fperm;
  t->lname=x->lname;
  t->lperm=x->lperm;
  t->fusrname=x->fusrname;
  t->fgrpname=x->fgrpname;
  t->lusrname=x->lusrname;
  t->lgrpname=x->lgrpname;
  t->uid=x->uid;
  t->gid=x->gid;
  t->fatime=x->fatime;
  t->fmtime=x->fmtime;
  t->fctime=x->fctime;
  t->latime=x->latime;
  t->lmtime=x->lmtime;
  t->lctime=x->lctime;
  t->u_read=x->u_read;
  t->u_write=x->u_write;
  t->u_exec=x->u_exec;
  t->g_read=x->g_read;
  t->g_write=x->g_write;
  t->g_exec=x->g_exec;
  t->o_read=x->o_read;
  t->o_write=x->o_write;
  t->o_exec=x->o_exec;
  t->size=x->size;
  t->is_dir=x->is_dir;
  t->is_lnk=x->is_lnk;
  t->blocks=x->blocks;
  t->fmode=x->fmode;
  t->lmode=x->lmode;
  t->fino=x->fino;
  t->lino=x->lino;

  //x->seqno=y->seqno;
  x->fstat=y->fstat;
  x->lstat=y->lstat;
  x->fname=y->fname;
  x->fperm=y->fperm;
  x->lname=y->lname;
  x->lperm=y->lperm;
  x->fusrname=y->fusrname;
  x->fgrpname=y->fgrpname;
  x->lusrname=y->lusrname;
  x->lgrpname=y->lgrpname;
  x->uid=y->uid;
  x->gid=y->gid;
  x->fatime=y->fatime;
  x->fmtime=y->fmtime;
  x->fctime=y->fctime;
  x->latime=y->latime;
  x->lmtime=y->lmtime;
  x->lctime=y->lctime;
  x->u_read=y->u_read;
  x->u_write=y->u_write;
  x->u_exec=y->u_exec;
  x->g_read=y->g_read;
  x->g_write=y->g_write;
  x->g_exec=y->g_exec;
  x->o_read=y->o_read;
  x->o_write=y->o_write;
  x->o_exec=y->o_exec;
  x->size=y->size;
  x->is_dir=y->is_dir;
  x->is_lnk=y->is_lnk;
  x->blocks=y->blocks;
  x->fmode=y->fmode;
  x->lmode=y->lmode;
  x->fino=y->fino;
  x->lino=y->lino;

  //y->seqno=t->seqno;
  y->fstat=t->fstat;
  y->lstat=t->lstat;
  y->fname=t->fname;
  y->fperm=t->fperm;
  y->lname=t->lname;
  y->lperm=t->lperm;
  y->fusrname=t->fusrname;
  y->fgrpname=t->fgrpname;
  y->lusrname=t->lusrname;
  y->lgrpname=t->lgrpname;
  y->uid=t->uid;
  y->gid=t->gid;
  y->fatime=t->fatime;
  y->fmtime=t->fmtime;
  y->fctime=t->fctime;
  y->latime=t->latime;
  y->lmtime=t->lmtime;
  y->lctime=t->lctime;
  y->u_read=t->u_read;
  y->u_write=t->u_write;
  y->u_exec=t->u_exec;
  y->g_read=t->g_read;
  y->g_write=t->g_write;
  y->g_exec=t->g_exec;
  y->o_read=t->o_read;
  y->o_write=t->o_write;
  y->o_exec=t->o_exec;
  y->size=t->size;
  y->is_dir=t->is_dir;
  y->is_lnk=t->is_lnk;
  y->blocks=t->blocks;
  y->fino=t->fino;
  y->lino=t->lino;
  y->fmode=t->fmode;
  y->lmode=t->lmode;

  t->fstat=NULL;
  t->lstat=NULL;
  t->next=NULL;
  t->prev=NULL;

  free(t);
  // ufu_free_entry(env,t);

}

int ufu_must_swap(struct s_env *env,struct s_entry *x,struct s_entry *y,int panel) {

  int swap,swap_test,is_dir_x,is_dir_y;
  char *s1,*s2;

  swap=FALSE;

  is_dir_x=S_ISDIR(x->lmode);
  is_dir_y=S_ISDIR(y->lmode);

  swap_test=(((is_dir_x)&&(is_dir_y))||((!is_dir_x)&&(!is_dir_y)));

  if(swap_test) {

    switch(env->panel[panel]->sortc) {

      case UFU_SORT_FNAME:
        s1=ufu_alloc_string(env,UFU_LEN_NAME);
        s2=ufu_alloc_string(env,UFU_LEN_NAME);
        strcpy(s1,x->fname);
        strcpy(s2,y->fname);
        switch(env->panel[panel]->sorto) {
          case UFU_SORT_ASC:
            if(strcmp(ufu_lower(env,s1),ufu_lower(env,s2))<0)
              swap=TRUE;
            break;
          case UFU_SORT_DSC:
            if(strcmp(ufu_lower(env,s1),ufu_lower(env,s2))>0)
              swap=TRUE;
            break;
        }
        ufu_free_string(env,s1);
        ufu_free_string(env,s2);
        break;

      case UFU_SORT_SIZE:
        switch(env->panel[panel]->sorto) {
          case UFU_SORT_ASC:
            if((x->size)<(y->size))
              swap=TRUE;
            break;
          case UFU_SORT_DSC:
            if((x->size)>(y->size))
              swap=TRUE;
            break;
        }
        break;

      case UFU_SORT_LA:
        switch(env->panel[panel]->sorto) {
          case UFU_SORT_ASC:
            if(x->fatime<y->fatime)
              swap=TRUE;
            break;
          case UFU_SORT_DSC:
            if(x->fatime>y->fatime)
              swap=TRUE;
            break;
        }
        break;

      case UFU_SORT_LM:
        switch(env->panel[panel]->sorto) {
          case UFU_SORT_ASC:
            if(x->fmtime<y->fmtime)
              swap=TRUE;
            break;
          case UFU_SORT_DSC:
            if(x->fmtime>y->fmtime)
              swap=TRUE;
            break;
        }
        break;

      case UFU_SORT_LC:
        switch(env->panel[panel]->sorto) {
          case UFU_SORT_ASC:
            if(x->fctime<y->fctime)
              swap=TRUE;
            break;
          case UFU_SORT_DSC:
            if(x->fctime>y->fctime)
              swap=TRUE;
            break;
        }
        break;

    }

  }

  return(swap);

}

void ufu_shuffle(struct s_env *env,int panel) {

  int is_dir,renumber,seqno;
  struct s_entry *e,*t1,*t2,*t3,*t4,*t5;

  sprintf(env->msg," Start reshuffling of %s",env->panel[panel]->dirname);
  ufu_log(env);

  sprintf(env->msg,"Reshuffling directory \"%s\"... ",env->panel[panel]->dirname);
  ufu_msg(env);

  renumber=FALSE;

  // Check if the "." entry is the first entry.
  e=env->panel[panel]->first;
  while((e!=NULL)&&(strcmp(e->fname,".")!=0)) {
    e=e->next;
  }

  if((e!=NULL)&&(e->seqno!=0)) {
    // Move "." to first place of list.

    sprintf(env->msg,"  Moved (first) entry \"%s\" from position %d.",e->fname,e->seqno);
    ufu_log(env);

    // First remove this entry from the list.
    if(e->next!=NULL) {
      e->prev->next=e->next;
      e->next->prev=e->prev;
    }
    else {
      e->prev->next=NULL;
    }

    // Then, insert the "." entry as first entry.
    e->next=env->panel[panel]->first;
    env->panel[panel]->first->prev=e;
    e->prev=NULL;
    env->panel[panel]->first=e;

    // We have moved ithe "." entry, so renumber the complete list.
    renumber=TRUE;

  }

  // Check if the ".." entry is the second entry.
  e=env->panel[panel]->first;
  while((e!=NULL)&&(strcmp(e->fname,"..")!=0)) {
    e=e->next;
  }

  if((e!=NULL)&&(e->seqno!=1)) {
    // Move ".." to second place of list.

    sprintf(env->msg,"  Moved (second) entry \"%s\" from position %d.",e->fname,e->seqno);
    ufu_log(env);

    // First remove this entry from the list.
    if(e->next!=NULL) {
      e->prev->next=e->next;
      e->next->prev=e->prev;
    }
    else {
      e->prev->next=NULL;
    }

    // Then, insert the ".." entry as second entry.

    t3=env->panel[panel]->first;
    t4=env->panel[panel]->first->next;
    t3->next=e;
    t4->prev=e;
    e->next=t4;
    e->prev=t3;

    // We have moved ithe ".." entry, so renumber the complete list.
    renumber=TRUE;

  }

  t1=env->panel[panel]->first;
  if(t1!=NULL) {
    is_dir=S_ISDIR(t1->fmode);
    while((t1!=NULL)&&(is_dir)) {
      t1=t1->next;
      if(t1!=NULL) {
        is_dir=S_ISDIR(t1->fmode);
      }
    }
  }

  if(t1!=NULL) {
    //t2=t1->next;
    // GdW-20080120: Aangepast.
    t2=t1->prev;

    while(t2!=NULL) {
      t5=t2->next;
      is_dir=((S_ISDIR(t2->fmode))||(S_ISDIR(t2->lmode)));
      if(is_dir) {

        sprintf(env->msg,"  Moving entry \"%s\".",t2->fname);
        ufu_log(env);

        renumber=TRUE;

        // Remove entry t2 from list.
        if((t2->prev!=NULL)&&(t2->next!=NULL)) {
          t2->prev->next=t2->next;
          t2->next->prev=t2->prev;
        }
        if((t2->prev==NULL)&&(t2->next!=NULL)) {
          env->panel[panel]->first=t2->next;
          t2->next->prev=NULL;
        }
        if((t2->prev!=NULL)&&(t2->next==NULL)) {
          env->panel[panel]->last=t2->prev;
          t2->prev->next=NULL;
        }

        // Insert entry t2 in list before entry t1.
        if(t1->prev!=NULL) {
          t3=t1->prev;
          t4=t1;
          t3->next=t2;
          t4->prev=t2;
          t2->prev=t3;
          t2->next=t4;
        }
        else {
          env->panel[panel]->first=t2;
          t1->prev=t2;
          t2->prev=NULL;
          t2->next=t1;
        }

      }
      t2=t5;
    }

  }

  if(renumber) {

    sprintf(env->msg,"  Start renumbering after reshuffle.");
    ufu_log(env);

    seqno=0;

    e=env->panel[panel]->first;
    while(e!=NULL) {
      env->panel[panel]->last=e;
      e->seqno=seqno++;
      e=e->next;
    }

    sprintf(env->msg,"  Finished renumbering after reshuffle.");
    ufu_log(env);

  }

  sprintf(env->msg," Finished reshuffling of %s",env->panel[panel]->dirname);
  ufu_log(env);

}

// Thnx to Simon Tatham!
void ufu_listsort(struct s_env *env,int panel) {

  int insize,nmerges,psize,qsize,i,seqno;
  struct s_entry *list,*tail,*p,*q,*e;

  sprintf(env->msg,"Sorting directory \"%s\"... ",env->panel[panel]->dirname);
  ufu_msg(env);

  list=env->panel[panel]->first;

  if(list!=NULL) {

    insize=1;

    while(TRUE) {

      p=list;
      list=NULL;
      tail=NULL;

      nmerges=0;

      while(p) {

        nmerges++;
        q=p;
        psize=0;

        for(i=0;i<insize;i++) {

          psize++;
          q=q->next;

          if(!q) {
            break;
          }

        }

        qsize=insize;

        while((psize>0)||((qsize>0)&&(q))) {

          if(psize==0) {
            e=q;
            q=q->next;
            qsize--;
          }
          else {
            if((qsize==0)||(!q)) {
              e=p;
              p=p->next;
              psize--;
            }
            else {
              if(ufu_must_swap(env,p,q,panel)) {
                e=p;
                p=p->next;
                psize--;
              }
              else {
                e=q;
                q=q->next;
                qsize--;
              }
            }
          }

          if(tail){
            tail->next=e;
          }
          else {
            list=e;
          }
          e->prev=tail;
          tail=e;

        }

        /* now p has stepped `insize' places along, and q has too */
        p=q;

      }

      tail->next=NULL;

      if(nmerges<=1) {
        /* allow for nmerges==0, the empty list case */

        i=0;

        env->panel[panel]->first=list;
        e=list;
        while(e!=NULL) {
          env->panel[panel]->last=e;
          e->seqno=i++;
          e=e->next;
        }

        return; 

      }
      else {

        /* Otherwise repeat, merging lists twice the size */
        insize *= 2;

      }

    }

    sprintf(env->msg,"  Start renumbering after sort.");
    ufu_log(env);

    seqno=0;

    e=env->panel[panel]->first;
    while(e!=NULL) {
      env->panel[panel]->last=e;
      e->seqno=seqno++;
      e=e->next;
    }

    sprintf(env->msg,"  Finished renumbering after sort.");
    ufu_log(env);

  }

  sprintf(env->msg," Finished sorting of %s",env->panel[panel]->dirname);
  ufu_log(env);

}

