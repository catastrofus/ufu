
#include "ufu.h"
#include "ufu.p"

int ufu_mark_level(struct s_env *env,char *s) {

  int level;
  char *p;

  if(strlen(s)>0) {
    p=s;
    level=0;
    while((p=strstr(p,"/"))!=NULL) {
      level++;
      p++;
    }
    if(env->debug) {
      sprintf(env->msg," Level found for \"%s\": %d",s,level);
      ufu_log(env);
    }
    return(--level);
  }
  else {
    return(-1);
  }
}

int ufu_add_mark(struct s_env *env,int panel,struct s_entry *cos) {

  int marked;
  char *abs_name,*dname;

  if((env->mlast==NULL)||((env->mlast!=NULL)&&((env->mlast->seqno+1)<env->max_to_mark))) {

    marked=FALSE;
    sprintf(env->msg,"Marking... ");
    ufu_msg(env);

    switch(ufu_can_mark(env,cos)) {
      case UFU_IS_NOT:
        break;
      case UFU_IS_REG:
          ufu_mark(env,panel,env->panel[panel]->dirname,cos,&marked);
        break;
      case UFU_IS_DIR:
        if(env->confirmaction) {
          sprintf(env->msg,"%s, do you wish to mark this humble directory? ",env->master);
          if(ufu_get_y(env)) {
            abs_name=ufu_alloc_string(env,UFU_LEN_NAME);
            if(env->panel[panel]->remote) {
              dname=ufu_alloc_string(env,UFU_LEN_NAME);
              strcpy(dname,env->panel[panel]->dirname);
              ufu_concat_dir(env,abs_name,env->panel[panel]->dirname,cos->fname);
              ufu_mark(env,panel,abs_name,NULL,&marked);
              ufu_free_string(env,abs_name);
              ufu_free_entries(env,panel);
              ufu_com_read_dir(env,dname,panel);
              ufu_free_string(env,dname);
            }
            else {
              ufu_concat_dir(env,abs_name,env->panel[panel]->dirname,cos->fname);
              ufu_mark(env,panel,abs_name,NULL,&marked);
              ufu_free_string(env,abs_name);
            }
          }
          else {
            abs_name=ufu_alloc_string(env,UFU_LEN_NAME);
            if(env->panel[panel]->remote) {
              dname=ufu_alloc_string(env,UFU_LEN_NAME);
              strcpy(dname,env->panel[panel]->dirname);
              ufu_concat_dir(env,abs_name,env->panel[panel]->dirname,cos->fname);
              ufu_mark(env,panel,abs_name,NULL,&marked);
              ufu_free_string(env,abs_name);
              ufu_free_entries(env,panel);
              ufu_com_read_dir(env,dname,panel);
              ufu_free_string(env,dname);
            }
            else {
              ufu_concat_dir(env,abs_name,env->panel[panel]->dirname,cos->fname);
              ufu_mark(env,panel,abs_name,NULL,&marked);
              ufu_free_string(env,abs_name);
            }
          }
        }
        break;
    }

  }
  else {

    ufu_get_key(env,UFU_MAX_TO_MARK,NULL);

  }

  return(marked);

}

int ufu_can_mark(struct s_env *env,struct s_entry *e) {

  int mark;

  mark=UFU_IS_NOT;
  if((env->mlast==NULL)||((env->mlast!=NULL)&&((env->mlast->seqno+1)<env->max_to_mark))) {
    if(e!=NULL) {
      if(strcmp(e->fname,".")!=0) {
        if(strcmp(e->fname,"..")!=0) {
          if(!S_ISLNK(e->lmode)) {
            if(S_ISREG(e->fmode)) {
              mark=UFU_IS_REG;
            }
            if(S_ISDIR(e->fmode)) {
              if(env->incsubdir) {
                mark=UFU_IS_DIR;
              }
            }
          }
        }
      }
    }
  }

  return(mark);

}

int ufu_mark(struct s_env *env,int panel,char *dname,struct s_entry *entry,int *marked) {

  char *abs_name;

  struct s_entry *e;
  struct s_mark *m,*n;
  struct dirent *de;

  DIR *dirp;

  abs_name=ufu_alloc_string(env,UFU_LEN_NAME);

  if(entry!=NULL) {
    // Object is a regular file.
    ufu_concat_dir(env,abs_name,dname,entry->fname);
    if(env->debug) {
      sprintf(env->msg,"Marking file \"%s\".",abs_name);
    }
    sprintf(env->msg,"Marking file \"%s\" ",abs_name);
    ufu_msg(env);

    m=ufu_alloc_mark(env);
    m->entry=ufu_alloc_entry(env);
    m->entry->fstat=ufu_alloc_stat(env);
    m->entry->lstat=ufu_alloc_stat(env);
    m->panel=panel;
    m->level=ufu_mark_level(env,abs_name);
    strcpy(m->dname,dname);
    ufu_copy_marked(env,m,entry);

    if(env->mfirst==NULL) {
      m->seqno=0;
      env->mfirst=m;
      env->mlast=m;
      m->prev=NULL;
      m->next=NULL;
      *marked=TRUE;
    }
    else {
      if((env->mlast==NULL)||((env->mlast!=NULL)&&((env->mlast->seqno+1)<env->max_to_mark))) {
        n=env->mlast;
        m->seqno=n->seqno+1;
        n->next=m;
        m->prev=n;
        m->next=NULL;
        env->mlast=m;
        *marked=TRUE;
      }
      else {
        sprintf(env->msg," Ignoring entry \"%s\"!",abs_name);
        ufu_log(env);
        sprintf(env->msg," Too many marked entries (>%d)",env->max_to_mark);
        ufu_log(env);
        return(FALSE);
      }
    }
  }
  else {
    // Object is a directory.
    if(env->incsubdir) {
      if((env->mlast==NULL)||((env->mlast!=NULL)&&((env->mlast->seqno+1)<env->max_to_mark))) {
        if(env->panel[panel]->remote) {
          // Remote.
          if(ufu_com_read_dir(env,dname,panel)) {
            e=env->panel[panel]->first;
            while(e!=NULL) {
              ufu_concat_dir(env,abs_name,dname,e->fname);
              switch(ufu_can_mark(env,e)) {
                case UFU_IS_NOT:
                  // Not a regular file or directory!
                  sprintf(env->msg," Ignoring \"%s\"!",abs_name);
                  ufu_log(env);
                  break;
                case UFU_IS_REG:
                  ufu_mark(env,panel,dname,e,marked);
                  break;
                case UFU_IS_DIR:
                  sprintf(env->msg," Entering directory \"%s\" recursively.",abs_name);
                  ufu_log(env);
                  ufu_mark(env,panel,abs_name,NULL,marked);
                  break;
              }
              e=e->next;
            }
            ufu_free_entries(env,panel);
            ufu_com_read_dir(env,dname,panel);
          }
        }
        else {
          if((dirp=opendir(dname))!=NULL) {
            de=ufu_alloc_dirent(env);
            while((de=readdir(dirp))!=NULL) {
              e=ufu_alloc_entry(env);
              ufu_concat_dir(env,e->fname,dname,de->d_name);
              ufu_stat_entry(env,env->mpanel+1,e);
              strcpy(e->fname,de->d_name);
              e->next=NULL;
              e->prev=NULL;
              ufu_concat_dir(env,abs_name,dname,e->fname);
              switch(ufu_can_mark(env,e)) {
                case UFU_IS_NOT:
                  // Not a regular file or directory!
                  sprintf(env->msg," Ignoring \"%s\"!",abs_name);
                  ufu_log(env);
                  break;
                case UFU_IS_REG:
                  ufu_mark(env,panel,dname,e,marked);
                  break;
                case UFU_IS_DIR:
                  sprintf(env->msg," Entering directory \"%s\" recursively.",abs_name);
                  ufu_log(env);
                  ufu_mark(env,panel,abs_name,NULL,marked);
                  break;
              }
              if(e!=NULL) {
                ufu_free_entry(env,e);
              }
            }
            ufu_free_dirent(env,de);
            closedir(dirp);
          }
        }
      }
      else {
        sprintf(env->msg," Too many marked entries (>%d)",env->max_to_mark);
        ufu_log(env);
        return(FALSE);
      }
    }

  }

  ufu_free_string(env,abs_name);

  return(TRUE);

}

void ufu_copy_marked(struct s_env *env,struct s_mark *m, struct s_entry *e) {

  // Manual copy of entry.
  m->entry->seqno=0;

  m->entry->uid=e->uid;
  m->entry->gid=e->gid;

  m->entry->fatime=e->fatime;
  m->entry->fmtime=e->fmtime;
  m->entry->fctime=e->fctime;

  m->entry->latime=e->latime;
  m->entry->lmtime=e->lmtime;
  m->entry->lctime=e->lctime;

  m->entry->u_read=e->u_read;
  m->entry->u_write=e->u_write;
  m->entry->u_exec=e->u_exec;

  m->entry->g_read=e->g_read;
  m->entry->g_write=e->g_write;
  m->entry->g_exec=e->g_exec;

  m->entry->o_read=e->o_read;
  m->entry->o_write=e->o_write;
  m->entry->o_exec=e->o_exec;

  m->entry->size=e->size;

  m->entry->fmode=e->fmode;
  m->entry->lmode=e->lmode;

  m->entry->blocks=e->blocks;
  m->entry->links=e->links;

  m->entry->is_dir=e->is_dir;
  m->entry->is_lnk=e->is_lnk;

  strcpy(m->entry->fname,e->fname);
  strcpy(m->entry->lname,e->lname);

  strcpy(m->entry->fperm,e->fperm);
  strcpy(m->entry->lperm,e->lperm);

  strcpy(m->entry->fusrname,e->fusrname);
  strcpy(m->entry->fgrpname,e->fgrpname);

  strcpy(m->entry->lusrname,e->lusrname);
  strcpy(m->entry->lgrpname,e->lgrpname);

  m->entry->next=NULL;
  m->entry->prev=NULL;

}

int ufu_rem_mark(struct s_env *env,struct s_mark *m,int rearrange) {

  int seqno;
  struct s_mark *tmp_m;

  sprintf(env->msg,"Wait while expunging marked entries");
  ufu_msg(env);

  sprintf(env->msg,"Unmarking file \"%s\" ",m->entry->fname);
  ufu_log(env);

  if(m->prev!=NULL) {
    m->prev->next=m->next;
  }
  else {
    env->mfirst=m->next;
  }
  if(m->next!=NULL) {
    m->next->prev=m->prev;
  }
  else {
    env->mlast=m->prev;
  }
  ufu_free_mark(env,m);

  if(rearrange) {

    sprintf(env->msg,"Rearranging list of marked entries.");
    ufu_log(env);

    seqno=0;
    tmp_m=env->mfirst;
    while(tmp_m!=NULL) {
      tmp_m->seqno=seqno++;
      tmp_m=tmp_m->next;
    }

    sprintf(env->msg,"Finished rearranging list of marked entries.");
    ufu_log(env);

  }

  return(TRUE);

}

void ufu_rem_mark_from_panel(struct s_env *env,int panel) {

  struct s_mark *m;

  m=env->mfirst;
  while(m!=NULL) {

    if(m->panel==panel) {

      ufu_rem_mark(env,m,FALSE);

    }

    m=m->next;

  }

}

void ufu_show_mark(struct s_env *env) {

  int again,redraw,key,row,rows,seqno,seq_tos,seq_bos,i,r,lineno,panel,len,width;
  char *fname,*s,*inp;
  struct s_mark *m,*cos,*tos;

  fname=ufu_alloc_string(env,UFU_LEN_NAME);
  s=ufu_alloc_string(env,UFU_LEN_NAME);

  panel=0;

  width=env->cols;

  rows=env->rows-6;

  tos=env->mfirst;
  cos=env->mfirst;

  seqno=env->hist_mark;
  m=env->mfirst;
  while((m!=NULL)&&(m->seqno!=seqno)) {
    m=m->next;
  }

  if(m!=NULL) {
    cos=m;
    tos=m;

    r=rows/2;
    while((m->prev!=NULL)&&(r>0)) {
      r--;
      tos=m;
      m=m->prev;
    }
 
  }
  else {
    m=env->mfirst;
  }

  redraw=TRUE;
  again=(m!=NULL);

  while((again)&&(env->mfirst!=NULL)) {

    if(redraw) {

      ufu_wclear(env->top);
      mvwprintw(env->top,0,0,"[%s] Show marked entries",env->nodename);

      ufu_wrefresh(env->top);

      ufu_wclear(env->bottom);
      ufu_wrefresh(env->bottom);

      ufu_wclear(env->body);
      mvwprintw(env->body,0,1,"SeqNo");
      mvwprintw(env->body,0,7,"Panel");
      mvwprintw(env->body,0,13,"Name");
      mvwprintw(env->body,0,68+(width-UFU_MIN_COLS)+7,"Size");
      mvwprintw(env->body,1,1,"----- -----");
      mvwprintw(env->body,1,61+(width-UFU_MIN_COLS)+7,"-----------");
      for(i=0;i<(54+(width-UFU_MIN_COLS));i++) {
        mvwprintw(env->body,1,13+i,"-");
      }
      ufu_wrefresh(env->body);

    }

    m=tos;
    row=1;

    while(row<=rows) {

      if(m!=NULL) {

        if(m==cos)  wattron(env->body,A_REVERSE);
        mvwprintw(env->body,row+1,1,"%5d",m->seqno);
        mvwprintw(env->body,row+1,7," %3d ",m->panel);

        ufu_concat_dir(env,fname,m->dname,m->entry->fname);

        ufu_resize_string(env,fname,54+(width-UFU_MIN_COLS),s);
        mvwprintw(env->body,row+1,13,"%-s",s);
        for(i=strlen(s);i<(54+(width-UFU_MIN_COLS));i++) {
          mvwprintw(env->body,row+1,13+i," ");
        }

        if(S_ISDIR(m->entry->lmode)) {
          mvwprintw(env->body,row+1,68+(width-UFU_MIN_COLS),"           ");
        }
        else {
          mvwprintw(env->body,row+1,68+(width-UFU_MIN_COLS),"%11d",m->entry->size);
        }

        if(m==cos)  wattroff(env->body,A_REVERSE);
        m=m->next;
      }

      row++;

    }

    ufu_wrefresh(env->body);

    if(strlen(cos->msg)>0)
      key=ufu_get_key(env,UFU_NO_TEXT,cos->msg);
    else
      key=ufu_get_key(env,UFU_NO_TEXT,NULL);

    switch(key) {

      case UFU_KEY_QUIT:
        ufu_add_hist(env,UFU_HIST_MARK,NULL,cos->seqno);
        again=FALSE;
        break;

      case UFU_KEY_HELP:
      case UFU_KEY_F1:
        env->key_help++;
        ufu_help(env,env->mpanel+1,UFU_HELP_MARK);
        redraw=TRUE;
        break;

      case UFU_KEY_INFO:
      case UFU_KEY_F2:
        env->key_info++;
        ufu_info_entry(env,cos->entry,cos->panel,cos,NULL,UFU_INFO_MARK);
        redraw=TRUE;
        break;

      case UFU_KEY_DOWN:
        env->key_down++;
        if(cos->next!=NULL) {
          seq_tos=tos->seqno;
          seq_bos=seq_tos+rows-1;
          cos=cos->next;
          if(cos->seqno>seq_bos) {
            tos=cos;
          }
        }
        redraw=TRUE;
        break;

      case UFU_KEY_UP:
        env->key_up++;
        if(cos->prev!=NULL) {
          seq_tos=tos->seqno;
          seq_bos=seq_tos+rows-1;
          cos=cos->prev;
          if(cos->seqno<seq_tos) {
            i=0;
            while((i<rows)&&(tos->prev!=NULL)) {
              tos=tos->prev;
              i++;
            }
          }
        }
        break;

      case UFU_KEY_FIRST:
      case UFU_KEY_HOME:
        env->key_first++;
        tos=env->mfirst;
        cos=env->mfirst;
        break;

      case UFU_KEY_LAST:
      case UFU_KEY_END:
        env->key_last++;
        while(cos->next!=NULL) {
          cos=cos->next;
        }
        //env->panel[panel]->cos=cos;
        //env->panel[panel]->tos=cos;
        tos=cos;
        i=0;
        while((i<(rows-1))&&(tos->prev!=NULL)) {
          tos=tos->prev;
          i++;
        }
        break;

      case UFU_KEY_NEXTPAGE:
      case KEY_NPAGE:
        env->key_next_page++;
        i=0;
        while((i<rows)&&(tos->next!=NULL)) {
          tos=tos->next;
          i++;
        }
        cos=tos;
        break;

      case UFU_KEY_PREVPAGE:
      case KEY_PPAGE:
        env->key_prev_page++;
        i=0;
        while((i<rows)&&(tos->prev!=NULL)) {
          tos=tos->prev;
          i++;
        }
        cos=tos;
        break;

      case UFU_KEY_MARK:
      case UFU_KEY_ENTER:
      case UFU_KEY_SELECT:
        env->key_rem_mark++;
        m=cos;
        // What to do if there is no next entry?
        if(cos->next!=NULL) {
          if(cos->prev==NULL) {
            env->mfirst=cos->next;
          }
          if(tos==cos) {
            tos=cos->next;
          }
          cos=cos->next;
        }
        else {
          // What to do if there is no previous entry?
          if(cos->prev!=NULL) {
            env->mlast=cos->prev;
            cos=cos->prev;
            if(cos->seqno<seq_tos) {
              i=0;
              while((i<rows)&&(tos->prev!=NULL)) {
                tos=tos->prev;
                i++;
              }
            }
          }
          else {
            // No next or previous entry...
            env->mfirst=NULL;
            env->mlast=NULL;
            again=FALSE;
          }
        }
        ufu_rem_mark(env,m,TRUE);
        //seq_cos=cos->seqno;
        seq_tos=tos->seqno;
        redraw=TRUE;
        again=(env->mfirst!=NULL);
        break;

      case UFU_KEY_EDIT:
        env->key_edit++;
        if(env->panel[cos->panel]->remote) {
          ufu_com_get_txtfile(env,cos->dname,cos->entry->fname,cos->panel,UFU_EDIT);
        }
        else {
          ufu_cmd_edit(env,cos->dname,cos->entry->fname);
        }
        break;

      case UFU_KEY_VIEW:
        env->key_view++;
        if(env->panel[cos->panel]->remote) {
          ufu_com_get_txtfile(env,cos->dname,cos->entry->fname,cos->panel,UFU_VIEW);
        }
        else {
          ufu_cmd_view(env,cos->dname,cos->entry->fname);
        }
        break;

      case UFU_KEY_GO:
        env->key_go++;
        inp=ufu_alloc_string(env,6);
        sprintf(env->msg,"%s, enter your seqno:",env->master);
        ufu_msg(env);
        ufu_clear_string(env,inp,6);
        ufu_rl(env,env->bottom,strlen(env->msg)+1,0,TRUE,5,5,inp,TRUE,FALSE);
        lineno=atoi(inp);

        env->m_cos=cos;
        env->m_tos=tos;

        ufu_goto_seqno(env,UFU_SEARCH_MARK,panel,lineno);

        cos=env->m_cos;
        tos=env->m_tos;

        ufu_free_string(env,inp);
        break;

      case UFU_KEY_CONFIG:
        env->key_config++;
        ufu_show_config(env);
        redraw=TRUE;
        break;

      case UFU_KEY_SETTING:
        env->key_setting++;
        ufu_show_setting(env);
        redraw=TRUE;
        break;

      case UFU_KEY_LOG:
        env->key_view++;
        ufu_cmd_view(env,env->logname,NULL);
        break;

      case UFU_KEY_EXPUNGE:
        ufu_expunge_mark(env);
        break;

      case UFU_KEY_SEARCH:

        env->key_search++;
        sprintf(env->msg,"%s, enter pattern:",env->master);
        ufu_msg(env);
        len=env->cols-strlen(env->msg)-1;
        inp=ufu_alloc_string(env,len);
        ufu_clear_string(env,inp,len);
        ufu_rl(env,env->bottom,strlen(env->msg)+1,0,TRUE,len-1,len-1,inp,TRUE,FALSE);

        env->m_cos=cos;
        env->m_tos=tos;

        ufu_goto_fname(env,UFU_SEARCH_MARK,panel,inp);

        cos=env->m_cos;
        tos=env->m_tos;

        ufu_free_string(env,inp);

        break;

      case UFU_KEY_MARK_ACTION:

        env->key_show_mark_action++;
        ufu_mark_action_menu(env);
        redraw=TRUE;

        break;

      default:
        ufu_wrong_key(env);
        break;

    }

  }

  ufu_free_string(env,fname);
  ufu_free_string(env,s);

}

void ufu_expunge_mark(struct s_env *env) {

  struct s_mark *m,*m_next;

  sprintf(env->msg,"%s, do you wish to empty your remarkable list? ",env->master);
  if(ufu_get_y(env)) {
    env->key_expunge++;
    m=env->mfirst;
    while(m!=NULL) {
      m_next=m->next;
      ufu_rem_mark(env,m,FALSE);
      m=m_next;
    }
    env->mfirst=NULL;
    env->mlast=NULL;
  } 
}

