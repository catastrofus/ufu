
#include "ufu.h"
#include "ufu.p"

int ufu_edit_ucmd(struct s_env *env,struct s_ucmd *uc,int scope,int row,int col,int insert) {

  int ok;

  if(env->debug) {
    sprintf(env->msg,"UserCMD: Edit usercommand %d (%s).",uc->seqno,uc->exec);
  }

  ok=ufu_rl(env,env->body,col,row,insert,UFU_LEN_UCMD,UFU_LEN_UCMD_VIS,uc->exec,TRUE,TRUE);

  return(ok);

}

void ufu_add_ucmd(struct s_env *env,struct s_ucmd *uc,int scope) {

  struct s_ucmd *uc_tmp,*uclast;

  uc_tmp=ufu_alloc_ucmd(env);
  strcpy(uc_tmp->exec,uc->exec);
  uc_tmp->local=scope;

  if(env->uclast!=NULL) {

    uclast=env->uclast;
    uclast->next=uc_tmp;
    uc_tmp->prev=uclast;
    uc_tmp->seqno=uclast->seqno+1;
    env->uclast=uc_tmp;

  }
  else {

    uc_tmp->seqno=0;
    uc_tmp->next=NULL;
    uc_tmp->prev=NULL;
    env->ucfirst=uc_tmp;
    env->uclast=uc_tmp;

  }

  if(env->debug) {
    sprintf(env->msg,"UserCMD: Add new usercommand %d (%s).",uc_tmp->seqno,uc_tmp->exec);
  }

}

int ufu_rem_ucmd(struct s_env *env,struct s_ucmd *uc) {

  int seqno;

  if(env->debug) {
    sprintf(env->msg,"UserCMD: Delete usercommand %d (%s).",uc->seqno,uc->exec);
  }

  if(uc->prev==NULL) {
    // First entry.
    if(uc->next==NULL) {
      // And no entry left.
      env->ucfirst=NULL;
      env->uclast=NULL;
    }
    else {
      // Entry is the first of more entries.
      env->ucfirst=uc->next;
      uc->next->prev=NULL;
    }
  }
  else {
    // Not the first entry.
    if(uc->next==NULL) {
      // Last entry.
      env->uclast=uc->prev;
      uc->prev->next=NULL;
    }
    else {
      // Entry is one of more entries.
      uc->next->prev=uc->prev;
      uc->prev->next=uc->next;
    }
  }

  ufu_free_ucmd(env,uc);

  seqno=0;
  uc=env->ucfirst;
  while(uc!=NULL) {
    uc->seqno=seqno++;
    uc=uc->next;
  }

  return(TRUE);
}

void ufu_exec_ucmd(struct s_env *env,struct s_ucmd *uc,int panel,char *file) {

  int replaced;
  char *exec,*exec_tmp,*fn,*p;

  exec_tmp=ufu_alloc_string(env,UFU_LEN_UCMD);
  exec=ufu_alloc_string(env,UFU_LEN_UCMD);
  fn=ufu_alloc_string(env,UFU_LEN_NAME);

  replaced=0;

  strcpy(exec,uc->exec);

  // Construct absolute filename.
  ufu_concat_dir(env,fn,env->panel[panel]->dirname,file);

  if(strstr(exec,"{A}")!=NULL) {
    sprintf(env->msg,"UserCMD {A} replacement, command is: %s",exec);
    ufu_log(env);
    // ABSOLUTE filename replacement.
    while((p=strstr(exec,"{A}"))!=NULL) {
      replaced++;
      // Init var 'exec_tmp' with \0.
      ufu_clear_string(env,exec_tmp,UFU_LEN_UCMD);
      if(p!=exec) {
        strncat(exec_tmp,exec,(p-exec));
        strcat(exec_tmp,fn);
        strcat(exec_tmp,p+3);
      }
      else {
        strcat(exec_tmp,fn);
        strcat(exec_tmp,p+3);
      }
      sprintf(env->msg," UserCMD {A} replacement: %s",exec_tmp);
      ufu_log(env);
      strcpy(exec,exec_tmp);
    }
    sprintf(env->msg,"UserCMD {A} replacement, command is: %s",exec);
    ufu_log(env);
  }

  if(strstr(exec,"{R}")!=NULL) {
    sprintf(env->msg,"UserCMD {R} replacement, command is: %s",exec);
    ufu_log(env);
    // RELATIVE filename replacement.
    while((p=strstr(exec,"{R}"))!=NULL) {
      replaced++;
      // Init var 'exec_tmp' with \0.
      ufu_clear_string(env,exec_tmp,UFU_LEN_UCMD);
      if(p!=exec) {
        strncat(exec_tmp,exec,(p-exec));
        strcat(exec_tmp,file);
        strcat(exec_tmp,p+3);
      }
      else {
        strcat(exec_tmp,file);
        strcat(exec_tmp,p+3);
      }
      sprintf(env->msg," UserCMD {R} replacement: %s",exec_tmp);
      ufu_log(env);
      strcpy(exec,exec_tmp);
    }
    sprintf(env->msg,"UserCMD {R} replacement, command is: %s",exec);
    ufu_log(env);
  }

  if(strstr(exec,"{B}")!=NULL) {
    sprintf(env->msg,"UserCMD {B} replacement, command is: %s",exec);
    ufu_log(env);
    // BASENAME replacement.
    while((p=strstr(exec,"{B}"))!=NULL) {
      replaced++;
      // Init var 'exec_tmp' with \0.
      ufu_clear_string(env,exec_tmp,UFU_LEN_UCMD);
      if(p!=exec) {
        strncat(exec_tmp,exec,(p-exec));
        strcat(exec_tmp,env->panel[panel]->dirname);
        strcat(exec_tmp,p+3);
      }
      else {
        strcat(exec_tmp,env->panel[panel]->dirname);
        strcat(exec_tmp,p+3);
      }
      sprintf(env->msg," UserCMD {B} replacement: %s",exec_tmp);
      ufu_log(env);
      strcpy(exec,exec_tmp);
    }
    sprintf(env->msg,"UserCMD {B} replacement, command is: %s",exec);
    ufu_log(env);
  }

  if(replaced>0) {
    sprintf(env->msg,"UserCMD, %d replacements!",replaced);
    ufu_log(env);
  }
  else {
    sprintf(env->msg,"UserCMD, no replacements.");
    ufu_log(env);
    strcpy(exec,uc->exec);
    //strcat(cmd,"; read xyz");
  }

  sprintf(env->msg,"Executing usercommand from panel %d: %s.",panel,exec);
  ufu_log(env);
  ufu_cmd_exec(env,exec);

  ufu_get_key(env,UFU_IS_EXECUTED,NULL);

  ufu_free_string(env,fn);
  ufu_free_string(env,exec);
  ufu_free_string(env,exec_tmp);

}

void ufu_cmd_exec_key(struct s_env *env,int key,int panel,char *file) {

  int again;
  struct s_ucmd *uc;

  if(env->debug) {
    sprintf(env->msg,"UserCMD: Execute usercommand %d.",key);
    ufu_log(env);
  }

  uc=env->ucfirst;

  again=(uc!=NULL);

  while(again) {
    if(uc->seqno==key) {
      if(strlen(uc->exec)>0) {
        ufu_deinit_curses(env);
        ufu_exec_ucmd(env,uc,panel,file);
        ufu_init_curses(env);
        again=FALSE;
      }
      else {
        ufu_wrong_key(env);
      }
    }
    else {
      uc=uc->next;
      again=(uc!=NULL);
    }
  }

}

void ufu_show_ucmd(struct s_env *env,int panel,char *file) {

  int rem,again,changed,redraw,key,len,start,row,rows,seqno,seq_tos,seq_bos,i,r;
  struct s_ucmd *uc,*cos,*tos;

  tos=env->ucfirst;
  cos=env->ucfirst;

  uc=env->ucfirst;

  rows=env->rows-6;

  seqno=env->hist_ucmd;
  uc=env->ucfirst;
  while((uc!=NULL)&&(uc->seqno!=seqno)) {
    uc=uc->next;
  }

  if(uc!=NULL) {
    cos=uc;
    tos=uc;

    r=rows/2;
    while((uc->prev!=NULL)&&(r>0)) {
      r--;
      tos=uc;
      uc=uc->prev;
    }
 
  }
  else {
    uc=env->ucfirst;
  }

  changed=FALSE;
  again=(uc!=NULL);

  start=0;

  while(again) {

    if(redraw) {

      ufu_wclear(env->top);
      mvwprintw(env->top,0,0,"[%s] Show usercommands",env->nodename);

      ufu_wrefresh(env->top);

      ufu_wclear(env->bottom);
      ufu_wrefresh(env->bottom);

      ufu_wclear(env->body);
      mvwprintw(env->body,0,1,"SeqNo Command");
      mvwprintw(env->body,1,1,"----- ------------------------------------------------------------------------");
      ufu_wrefresh(env->body);

    }

    uc=tos;
    row=1;

    while(row<=rows) {

      if(uc!=NULL) {

        if(uc==cos)  wattron(env->body,A_REVERSE);

        mvwprintw(env->body,row+1,1,"%5d",uc->seqno);

        len=strlen(uc->exec);
        i=0;
        while(i<UFU_LEN_UCMD_VIS) {

          if((i+start)<len) {
            mvwprintw(env->body,row+1,7+i,"%c",uc->exec[i+start]);
          }
          else {
            mvwprintw(env->body,row+1,7+i," ");
          }

          i++;

        }

        if(uc==cos)  wattroff(env->body,A_REVERSE);

        uc=uc->next;

      }

      row++;

    }

    ufu_wrefresh(env->body);

    key=ufu_get_key(env,UFU_NO_TEXT,NULL);

    switch(key) {

      case UFU_KEY_QUIT:
        ufu_add_hist(env,UFU_HIST_UCMD,NULL,cos->seqno);
        again=FALSE;
        if(changed) {
            ufu_write_config(env);
        }
        break;

      case UFU_KEY_SELECT:
      case UFU_KEY_ENTER:
        env->key_select++;
        if(cos->seqno>0) {
          ufu_add_hist(env,UFU_HIST_UCMD,NULL,cos->seqno);
          // Execute usercommand.
          ufu_cmd_exec_key(env,cos->seqno,panel,file);
        }
        break;

      case UFU_KEY_HELP:
      case UFU_KEY_F1:
        env->key_help++;
        ufu_help(env,env->mpanel+1,UFU_HELP_UCMD);
        redraw=TRUE;
        break;

      case UFU_KEY_LEFT:
        env->key_left++;
        if(start>=UFU_LEN_RL_STEP) {
          start=start-UFU_LEN_RL_STEP;
        }
        break;

      case UFU_KEY_RIGHT:
        env->key_right++;
        if(start<(UFU_LEN_UCMD-UFU_LEN_UCMD_VIS-UFU_LEN_RL_STEP)) {
          start=start+UFU_LEN_RL_STEP;
        }
        break;

      case UFU_KEY_DOWN:
        env->key_down++;
        if(cos->next!=NULL) {
          seq_tos=tos->seqno;
          seq_bos=seq_tos+rows-1;
          cos=cos->next;
          if(cos->seqno>seq_bos) {
            tos=cos;
          }
        }
        redraw=TRUE;
        break;

      case UFU_KEY_UP:
        env->key_up++;
        if(cos->prev!=NULL) {
          seq_tos=tos->seqno;
          seq_bos=seq_tos+rows-1;
          cos=cos->prev;
          if(cos->seqno<seq_tos) {
            i=0;
            while((i<rows)&&(tos->prev!=NULL)) {
              tos=tos->prev;
              i++;
            }
          }
        }
        break;

      case UFU_KEY_FIRST:
      case UFU_KEY_HOME:
        env->key_first++;
        tos=env->ucfirst;
        cos=env->ucfirst;
        break;

      case UFU_KEY_LAST:
      case UFU_KEY_END:
        env->key_last++;
        while(cos->next!=NULL) {
          cos=cos->next;
        }
        tos=cos;
        i=0;
        while((i<(rows-1))&&(tos->prev!=NULL)) {
          tos=tos->prev;
          i++;
        }
        break;

      case UFU_KEY_NEXTPAGE:
        env->key_next_page++;
        i=0;
        while((i<rows)&&(tos->next!=NULL)) {
          tos=tos->next;
          i++;
        }
        cos=tos;
        break;

      case UFU_KEY_PREVPAGE:
        env->key_prev_page++;
        i=0;
        while((i<rows)&&(tos->prev!=NULL)) {
          tos=tos->prev;
          i++;
        }
        cos=tos;
        break;

      case UFU_KEY_DELETE:
      case UFU_KEY_ERASE:
        env->key_rem_ucmd++;
        if(cos->seqno>0) {
          // Delete usercommand.
          rem=!env->confirmremove;
          if(env->confirmremove) {
            sprintf(env->msg,"%s, are you sure to remove this humble (%d) entry? ",env->master,cos->seqno);
            rem=(ufu_get_yn(env));
          }
          if(rem) {
            changed=TRUE;
            uc=cos;
            if(cos->next!=NULL) {
              seq_tos=tos->seqno;
              seq_bos=seq_tos+rows-1;
              if(seq_bos>env->uclast->seqno) {
                seq_bos=env->uclast->seqno;
              }
              if(tos==cos) {
                cos=cos->next;
                tos=cos;
                seq_tos=tos->seqno;
              }
              else {
                cos=cos->next;
              }
              if(cos->seqno>seq_bos) {
                tos=cos;
              }
            }
            else {
              if(cos->prev!=NULL) {
                cos->prev->next=NULL;
                cos=env->ucfirst;
                while(cos->next!=NULL) {
                  cos=cos->next;
                }
                tos=cos;
                i=0;
                while((i<(rows-1))&&(tos->prev!=NULL)) {
                  tos=tos->prev;
                  i++;
                }
              }
              else {
                again=FALSE;
              }
            }
            ufu_rem_ucmd(env,uc);
            if((changed)&&(!again)) {
              ufu_write_config(env);
            }
          }
        }
        break;

      case UFU_KEY_INSERT:
      case UFU_KEY_APPEND:
        env->key_add_ucmd++;
        // Add new usercommand.
        redraw=TRUE;
        uc=ufu_alloc_ucmd(env);
        strcpy(uc->exec,"");
        ufu_add_ucmd(env,uc,TRUE);
        ufu_free_ucmd(env,uc);

        cos=env->uclast;
        tos=cos;
        i=0;
        while((i<(rows-1))&&(tos->prev!=NULL)) {
          tos=tos->prev;
          i++;
        }

        uc=tos;
        row=1;
        while(row<=rows) {
          if(uc!=NULL) {
            mvwprintw(env->body,row+1,1,"%5d",uc->seqno);
            if(uc==cos)  wattron(env->body,A_REVERSE);
            len=strlen(uc->exec);
            i=0;
            while(i<UFU_LEN_UCMD_VIS) {
              if((i+start)<len) {
                mvwprintw(env->body,row+1,7+i,"%c",uc->exec[i+start]);
              }
              else {
                mvwprintw(env->body,row+1,7+i," ");
              }
              i++;
            }
            if(uc==cos)  wattroff(env->body,A_REVERSE);
            uc=uc->next;
          }
          row++;
        }
        ufu_wrefresh(env->body);

        changed=ufu_edit_ucmd(env,cos,cos->local,2+(cos->seqno)-(tos->seqno),7,TRUE);
        if(strlen(cos->exec)==0) {
          uc=cos;
          if(uc->next!=NULL) {
            uc=uc->next;
          }
          else {
            if(uc->prev!=NULL) {
              uc=uc->prev;
            }
          }
          ufu_rem_ucmd(env,cos);
          cos=uc;
          changed=FALSE;
        }

        break;

      case UFU_KEY_EDIT:
        env->key_edit++;
        if(cos->seqno>0) {
          // Edit usercommand.
          if(cos->local) {
            ufu_edit_ucmd(env,cos,cos->local,2+(cos->seqno)-(tos->seqno),7,FALSE);
            if(strlen(cos->exec)==0) {
              ufu_rem_ucmd(env,cos);
            }
            changed=TRUE;
          }
        }
        break;

      default:
        ufu_wrong_key(env);
        break;

    }

  }

}
