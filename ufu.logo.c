
#include "ufu.h"
#include "ufu.p"

void ufu_logo(struct s_env *env) {

  int row;
  char *s;

  row=(env->rows-18)/2-1;
  if(row>10)  row=10;

  ufu_clear(env);

  mvwprintw(env->top,0,(env->cols-20)/2,"an Unix File Utility");

  mvwprintw(env->body,row,(env->cols-10)/2,"Welcome to");

  mvwprintw(env->body,row+2,(env->cols-17)/2,"U   U FFFFF U   U");
  mvwprintw(env->body,row+3,(env->cols-17)/2,"U   U F     U   U");
  mvwprintw(env->body,row+4,(env->cols-17)/2,"U   U F     U   U");
  mvwprintw(env->body,row+5,(env->cols-17)/2,"U   U FFF   U   U");
  mvwprintw(env->body,row+6,(env->cols-17)/2,"U   U F     U   U");
  mvwprintw(env->body,row+7,(env->cols-17)/2,"U   U F     U   U");
  mvwprintw(env->body,row+8,(env->cols-17)/2," UUU  F      UUU");

  s=ufu_alloc_string(env,strlen(UFU_COPYRIGHT));
  strcpy(s,UFU_COPYRIGHT);
  ufu_lower(env,s);
  mvwprintw(env->body,row+10,(env->cols-strlen(s))/2,s);
  ufu_free_string(env,s);

  s=ufu_alloc_string(env,strlen(UFU_VERSION));
  strcpy(s,UFU_VERSION);
  ufu_lower(env,s);
  mvwprintw(env->body,row+12,(env->cols-strlen(s))/2,s);
  ufu_free_string(env,s);

  s=ufu_alloc_string(env,strlen(UFU_BUILDDATE));
  strcpy(s,UFU_BUILDDATE);
  ufu_lower(env,s);
  mvwprintw(env->body,row+14,(env->cols-strlen(s))/2,s);
  ufu_free_string(env,s);

  mvwprintw(env->body,row+17,(env->cols-45)/2,"Remember, you can always use '%c' to get help!",UFU_KEY_QMARK);

  ufu_wrefresh(env->top);
  ufu_wrefresh(env->body);

  // Read the defined panels while the user is looking at the logo... :-)
  ufu_read_panels(env);

  sprintf(env->msg,"Press <y> if you want to look at the copyright notice: ");
  if(ufu_get_y(env)) {

    ufu_clear(env);

    mvwprintw(env->top,0,(env->cols-20)/2,"an Unix File Utility");

    mvwprintw(env->body,row,(env->cols-10)/2,"Welcome to");

    mvwprintw(env->body,row+2,(env->cols-17)/2,"U   U FFFFF U   U");
    mvwprintw(env->body,row+3,(env->cols-17)/2,"U   U F     U   U");
    mvwprintw(env->body,row+4,(env->cols-17)/2,"U   U F     U   U");
    mvwprintw(env->body,row+5,(env->cols-17)/2,"U   U FFF   U   U");
    mvwprintw(env->body,row+6,(env->cols-17)/2,"U   U F     U   U");
    mvwprintw(env->body,row+7,(env->cols-17)/2,"U   U F     U   U");
    mvwprintw(env->body,row+8,(env->cols-17)/2," UUU  F      UUU");

    mvwprintw(env->body,row+10,1,"This program is free software: you can redistribute it and/or modify it under ");
    mvwprintw(env->body,row+11,1,"the terms of the GNU General Public License as published by the Free Software");
    mvwprintw(env->body,row+12,1,"Foundation, either version 3 of the License, or any later version.");


    mvwprintw(env->body,row+14,1,"This program is distributed in the hope that it will be useful, but WITHOUT");
    mvwprintw(env->body,row+15,1,"ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS");
    mvwprintw(env->body,row+16,1,"FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.");

    ufu_wrefresh(env->top);
    ufu_wrefresh(env->body);

    ufu_any_key(env);

  }

}
