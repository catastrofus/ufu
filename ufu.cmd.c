
#include "ufu.h"
#include "ufu.p"

void ufu_cmd_view(struct s_env *env,char *dname,char *fname) {

  ufu_deinit_curses(env);
  ufu_exec_ext(env,env->viewer,dname,fname);
  ufu_init_curses(env);

}

void ufu_cmd_edit(struct s_env *env,char *dname,char *fname) {

  ufu_deinit_curses(env);
  ufu_exec_ext(env,env->editor,dname,fname);
  ufu_init_curses(env);

}

void ufu_cmd_exec(struct s_env *env,char *cmd) {

  ufu_deinit_curses(env);
  ufu_exec_ext(env,cmd,NULL,NULL);
  ufu_init_curses(env);

}

